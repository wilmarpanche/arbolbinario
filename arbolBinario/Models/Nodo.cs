﻿
using System;

namespace arbolBinario.Models
{
    public class Nodo
    {
        public int? valor { get; set; }
        public Nodo nodoIzquierda { get; set; }
        public Nodo nodoDerecha { get; set; }

        public void insertar(int valor)
        {
            try
            {

                if (this.valor == null)
                {
                    this.valor = valor;
                }
                else
                {
                    if (valor < this.valor)
                    {
                        if (this.nodoIzquierda == null)
                            this.nodoIzquierda = new Nodo();
                        this.nodoIzquierda.insertar(valor);
                    }
                    else
                    {
                        if (this.nodoDerecha == null)
                            this.nodoDerecha = new Nodo();
                        this.nodoDerecha.insertar(valor);
                    }
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }
    }
}

﻿
using System;
using System.Collections.Generic;
using System.Linq;
using arbolBinario.Models;
using arbolBinario.Services;
using Microsoft.AspNetCore.Mvc;
namespace arbolBinario.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {

        [HttpPost("/ancestro")]
        public IActionResult buscarAncestro([FromBody]datosArbol entrada)
        {
            try
            {  
                Arbol nuevoArbol = new Arbol();
                int? ancestro = nuevoArbol.buscarAncestroCercano(entrada);
                return Ok(ancestro);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
            
        }

        [HttpPost("/nuevoarbol")]
        public IActionResult nuevoArbol([FromBody]List<int> arregloArbol)
        {
            try
            {
                Arbol nuevoArbol = new Arbol();
                Nodo arbol = nuevoArbol.crearArbolBinario(arregloArbol);
                return Ok(arbol);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

        }


    }
}

﻿using arbolBinario.Models;
using System;
using System.Collections.Generic;

namespace arbolBinario.Services
{
    public class Arbol
    {

        List<int?> ruta = new List<int?>();
        public Nodo crearArbolBinario(List<int> arbol)
        {
            try
            {
                Nodo nuevoArbol = new Nodo();
                foreach (var item in arbol)
                {
                    nuevoArbol.insertar(item);
                }
                return nuevoArbol;
            }
            catch (Exception e)
            {

                throw e;
            }
        }
        private List<int?> buscarNodo(Nodo Arbol, int nodoPerdido)
        {
            try
            {
                ruta = new List<int?>();
                return this.buscarNodoHijo(Arbol, nodoPerdido);
            }
            catch (Exception e)
            {
                throw e;
            }

        }


        private List<int?> buscarNodoHijo(Nodo Padre, int nodoPerdido)
        {
            try
            {
                if(Padre == null)
                {
                    ruta = new List<int?>();
                    return null;
                }
                if (Padre.valor > nodoPerdido)
                {
                    ruta.Add(Padre.valor);
                    this.buscarNodoHijo(Padre.nodoIzquierda, nodoPerdido);
                }
                else if (Padre.valor < nodoPerdido)
                {
                    ruta.Add(Padre.valor);
                    this.buscarNodoHijo(Padre.nodoDerecha, nodoPerdido);
                }
                return ruta;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public int? buscarAncestroCercano(datosArbol entrada)
        {
            try
            {
                if (entrada == null || entrada.arbol == null)
                    return null;
                
                List<int?> rutaUno = buscarNodo(entrada.arbol, entrada.nodoUno);
                List<int?> rutaDos = buscarNodo(entrada.arbol, entrada.nodoDos);
                int? ancestro = null;
                foreach (var item in rutaUno)
                {
                    if (rutaDos.Find(x => x.Value.Equals(item)) == null)
                        break;
                    ancestro = item;
                }
                return ancestro;
            }
            catch (Exception e)
            {

                throw e;
            }
        }


    }
}

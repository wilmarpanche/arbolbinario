using arbolBinario.Models;
using arbolBinario.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace arbolBinario.Test
{
    [TestClass]
    public class buscarAncestroCercanoTest
    {
        [TestMethod]
        public void buscarAncestroCercano()
        {
            Arbol nuevoArbol = new Arbol();
            datosArbol entrada = null;
            int? ancestro = null;
            List<int> arrarArbol = new List<int>();
            ancestro = nuevoArbol.buscarAncestroCercano(entrada);

            entrada = new datosArbol();
            ancestro = nuevoArbol.buscarAncestroCercano(entrada);

            entrada.arbol = nuevoArbol.crearArbolBinario(arrarArbol);
            ancestro = nuevoArbol.buscarAncestroCercano(entrada);


            arrarArbol.Add(67);
            arrarArbol.Add(39);
            arrarArbol.Add(44);
            arrarArbol.Add(28);
            arrarArbol.Add(29);
            arrarArbol.Add(76);
            arrarArbol.Add(74);
            arrarArbol.Add(85);
            arrarArbol.Add(83);
            arrarArbol.Add(87);
            entrada.arbol = nuevoArbol.crearArbolBinario(arrarArbol);
            entrada.nodoUno = 183;
            entrada.nodoDos = 187;

            ancestro = nuevoArbol.buscarAncestroCercano(entrada);

            ancestro = nuevoArbol.buscarAncestroCercano(entrada);
        }

        [TestMethod]
        public void crearArbolbinario() {

            Arbol nuevoArbol = new Arbol();
            List<int> arrarArbol = new List<int>();
            Nodo arbol = nuevoArbol.crearArbolBinario(arrarArbol);

            arrarArbol.Add(-67);
            arrarArbol.Add(0);
            arrarArbol.Add(44);
            arrarArbol.Add(28);
            arrarArbol.Add(29);
            arrarArbol.Add(76);
            arrarArbol.Add(74);
            arrarArbol.Add(85);
            arrarArbol.Add(83);
            arrarArbol.Add(87);
            arbol = nuevoArbol.crearArbolBinario(arrarArbol);


        }
    }
}

